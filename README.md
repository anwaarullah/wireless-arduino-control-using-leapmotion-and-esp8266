This repo contains the source code that allows to Wirelessly Control an Arduino(with ESP8266) using LeapMotion over WiFi.

The project writeup is available here: [https://anwaarullah.wordpress.com/2015/10/17/wireless-gesture-control-of-arduino-using-leapmotion-and-esp8266/](https://anwaarullah.wordpress.com/2015/10/17/wireless-gesture-control-of-arduino-using-leapmotion-and-esp8266/)