import java.io.IOException;
import java.io.DataOutputStream;
import java.net.Socket;

import com.leapmotion.leap.Controller;
import com.leapmotion.leap.Frame;
import com.leapmotion.leap.Listener;

class SampleListenerMain extends Listener {

	public void onInit(Controller controller) {
		System.out.println("Initialized");
	}

	public void onConnect(Controller controller) {
		System.out.println("Connected");
	}

	public void onDisconnect(Controller controller) {
		System.out.println("Disconnected");
	}

	public void onExit(Controller controller) {
		System.out.println("Exited");
	}
	public void onFrame(Controller controller) {
		Frame frame = controller.frame();
		System.out.println(frame.fingers().count());

		try {
			LeapIntoArduino.writeToArduino(frame.fingers().count());
			Thread.sleep(150);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class LeapIntoArduino {
	static Socket clientSocket = null;
	static DataOutputStream outToServer = null;

	public static void main(String[] args) throws Exception {

		clientSocket = new Socket("192.168.4.1", 80);
		outToServer = new DataOutputStream(clientSocket.getOutputStream());

		// Create a sample listener and controller
		SampleListenerMain listener = new SampleListenerMain();
		Controller controller = new Controller();

		// Have the sample listener receive events from the controller
		controller.addListener(listener);
		// Keep this process running until Enter is pressed
		System.out.println("Press Enter to quit...");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Remove the sample listener when done
		controller.removeListener(listener);
		clientSocket.close();
	}

	public static void writeToArduino(int noOfFingers)
	{	
		try {
				outToServer.writeBytes(" Finger:" +Integer.toString(noOfFingers));
				System.out.println("Data sent to Arduino:--> Finger:"  +Integer.toString(noOfFingers) + "<--");
			} catch (IOException e) { }
	}
}