#include <Servo.h> 
Servo myservo;  // create servo object to control a servo 
int pos = 0;    // variable to store the servo position 

void setup()
{
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);  
  resetGPIO();  
  Serial.begin(9600);
  Serial1.begin(9600);
  delay(1000);
  //establishWiFiConnection();  
  createWiFiHotspotServer();  
  delay(3000);
  //Blink LED 13 to signify all's good to go
}

void loop()
{
  if(Serial1.available())
  {
    if(Serial1.find("+IPD,"))
    {
      delay(200);
      Serial1.find("Finger:");
      int finger = Serial1.read()-48;
      Serial.println(finger);
      if(finger == 1)
      {
        digitalWrite(11, LOW);
      }
      if(finger == 3)
      {
        digitalWrite(12, LOW);
      }
      if(finger == 2)
      {
        digitalWrite(11, HIGH);
      }
      if (finger == 4)
      {
        digitalWrite(12, HIGH);
      }
      if (finger == 0)
      {
        myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
        myservo.write(0);              // tell servo to go to position in variable 'pos' 
        delay(500);
        myservo.detach();
      }      
      if (finger == 5)
      {
        myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
        myservo.write(180);              // tell servo to go to position in variable 'pos' 
        delay(500);
        myservo.detach();      
      }      
    }
  }
  delay(50);
}

void establishWiFiConnection() {
  Serial.print("Resetting ESP");
  Serial1.print("AT+RST\r\n");
  delay(7000);
  Serial.println("Shooting in first commands");
  testESPOnline();
  Serial.print("Connecting to WiFi");
  Serial1.print("AT+CWMODE=1\r\n");
  delay(3000);
  Serial1.print("AT+CWJAP=\"Potentiallabs\",\"someAwesomePasswordHere\"\r\n");
  delay(10000);
  readESPSerialResponse();  
  Serial1.print("AT+CIFSR\r\n");
  delay(2000);
  readESPSerialResponse();  
  Serial1.print("AT+CIPMUX=1\r\n");
  delay(2000);
  readESPSerialResponse();  
  delay(2000);  
  Serial1.print("AT+CIPSERVER=1,80\r\n");  
  readESPSerialResponse();   
  delay(3000);
  Serial1.print("AT+CIPSTO=28000\r\n");  
  readESPSerialResponse();   
  delay(3000);
  Serial1.print("AT\r\n"); 
  readESPSerialResponse(); 
  Serial1.print("AT\r\n");   
  if(Serial1.find("OK"))
  {
    multiBlink();
  }
  delay(3000);  
}

void createWiFiHotspotServer() {
  Serial.print("Resetting ESP");
  Serial1.print("AT+RST\r\n");
  delay(7000);
  Serial.println("Shooting in first commands");
  testESPOnline();
  Serial.print("Configuring AP Mode...");
  Serial1.print("AT+CWMODE=2\r\n");
  delay(4000);  
  Serial1.print("AT+CIPMUX=1\r\n");
  delay(2000);
  readESPSerialResponse();  
  delay(2000);  
  Serial1.print("AT+CIPSERVER=1,80\r\n");  
  readESPSerialResponse();   
  delay(5000);
  Serial1.print("AT+CIPSTO=28000\r\n");  
  readESPSerialResponse();
  delay(3000);  
  Serial1.print("AT+CIFSR\r\n");  
  readESPSerialResponse();     
  delay(3000);
  Serial1.print("AT\r\n"); 
  readESPSerialResponse(); 
  Serial1.print("AT\r\n");   
  if(Serial1.find("OK"))
  {
    multiBlink();
  }
  delay(3000);  
}

void resetWiFi() {
  Serial1.println("AT+RST");
}

void testESPOnline() {
  Serial1.print("AT\r\n");
  readESPSerialResponse();
  if(Serial1.find("OK"))
    Serial.println("All Good. ESP taking Commands");
  else
    Serial.println("Device Not Working! Please Reset");
}

void readESPSerialResponse() {
  while(Serial1.available())
  {
    Serial.write(Serial1.read());
  }
  Serial.println();
}

void resetGPIO()
{
  digitalWrite(11, LOW);
  digitalWrite(12, LOW);
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(11, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(13, LOW);  
}
void multiBlink()
{
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);    
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);    
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);    
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);    
  digitalWrite(13, HIGH);
  delay(100);
  digitalWrite(13, LOW);
  delay(100);        
}


